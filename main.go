package main

import (
	"bufio"
	"fmt"
	"math"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

type XYZ struct {
	X float64
	Y float64
	Z float64
}

func main() {

	el_f, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}

	el_r := bufio.NewReader(el_f)

	elScan := bufio.NewScanner(el_r)

	zs := []float64{}
	xyzs := []XYZ{}
	max := -math.MaxFloat64
	min := math.MaxFloat64
	for elScan.Scan() {

		text := strings.Trim(elScan.Text(), "\ufeff")
		split := strings.Split(text, " ")

		x, err := strconv.ParseFloat(split[0], 64)
		if err != nil {
			panic(err)
		}
		y, err := strconv.ParseFloat(split[1], 64)
		if err != nil {
			panic(err)
		}
		z, err := strconv.ParseFloat(split[2], 64)
		if err != nil {
			panic(err)
		}

		if z > max {
			max = z
		}
		if z < min {
			min = z
		}

		zs = append(zs, z)
		xyzs = append(xyzs, XYZ{
			X: x,
			Y: y,
			Z: z,
		})
	}
	el_f.Close()

	Shuffle(xyzs)

	out, err := os.Create(os.Args[1] + "_.csv")
	if err != nil {
		panic(err)
	}
	defer out.Close()
	wr := bufio.NewWriter(out)

	fmt.Println(min)
	fmt.Println(max)
	for i, xyz := range xyzs {
		normZ := (xyz.Z - min) / (max - min)
		xyzs[i].Z = normZ

		x := strconv.FormatFloat(xyzs[i].X, 'f', 6, 64)
		y := strconv.FormatFloat(xyzs[i].Y, 'f', 6, 64)
		z := strconv.FormatFloat(xyzs[i].Z, 'f', 6, 64)

		wr.WriteString(x + ";" + y + ";" + z + "\n")
		wr.Flush()
	}

}

func Shuffle(vals []XYZ) {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for len(vals) > 0 {
		n := len(vals)
		randIndex := r.Intn(n)
		vals[n-1], vals[randIndex] = vals[randIndex], vals[n-1]
		vals = vals[:n-1]
	}
}
