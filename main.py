import time
import sys
import csv
import os
import math
import operator

from colour import Color
from random import shuffle
from io import open

from sklearn.model_selection import StratifiedKFold
from sklearn.mixture import GaussianMixture
from sklearn.mixture import BayesianGaussianMixture
from sklearn import datasets
import numpy as np

from mpl_toolkits import mplot3d
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm


colors = ['deepskyblue', 'royalblue', 'navy', 'turquoise',
          'teal', 'lime', 'salmon', 'darkorange', 'orangered', 'red']

# colors = ['royalblue', 'turquoise', 'lime', 'darkorange', 'red']


def map_prob_to_pred(probs):
    pred = []
    for row in probs:
        index, val = max(enumerate(row), key=operator.itemgetter(1))
        pred.append(index)

    return pred


def make_weights(points, means, dxy, dz):
    weights = []
    for point in points:
        weight_vector = []
        for mean in means:
            fxy = (1+dxy)*(math.sqrt((mean[0]-point[0])
                                     ** 2 + (mean[1]-point[1])**2))
            fz = (1+dz)*math.sqrt((mean[2] - point[2])**2)
            weight = fxy + fz
            weight_vector.append(weight)

        w_sum = sum(weight_vector)
        for i in range(0, len(weight_vector)):
            weight_vector[i] = weight_vector[i]/w_sum

        weights.append(weight_vector)

    return weights


def weighted_probabilities(probs, points, means, dxy, dz):
    weighted_probs = []

    for i in range(0, len(probs)):
        point = points[i]
        point_probs = probs[i]
        point_wprobs = []
        for j in range(0, len(means)):
            mean = means[j]
            fxy = (math.sqrt((mean[0]-point[0]) ** 2 + (mean[1]-point[1])**2))
            fz = math.sqrt((mean[2] - point[2])**2)
            weight_multiplier = dxy * fxy + dz * fz
            point_wprobs.append(weight_multiplier * point_probs[j])

        wprobs_sum = sum(point_wprobs)
        for i in range(0, len(point_wprobs)):
            point_wprobs[i] = point_wprobs[i]/wprobs_sum

        weighted_probs.append(point_wprobs)

    return weighted_probs


def normalize(arr):
    ret = []
    for item in arr:
        ret.append((item - min(arr))/(max(arr) - min(arr)))

    return ret


def make_ellipses(gmm, ax):
    for n in range(gmm.n_components-1):
        if gmm.covariance_type == 'full':
            covariances = gmm.covariances_[n][:2, :2]
        elif gmm.covariance_type == 'tied':
            covariances = gmm.covariances_[:2, :2]
        elif gmm.covariance_type == 'diag':
            covariances = np.diag(gmm.covariances_[n][:2])
        elif gmm.covariance_type == 'spherical':
            covariances = np.eye(gmm.means_.shape[1]) * gmm.covariances_[n]
        v, w = np.linalg.eigh(covariances)
        u = w[0] / np.linalg.norm(w[0])
        angle = np.arctan2(u[1], u[0])
        angle = 180 * angle / np.pi  # convert to degrees
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        ell = mpl.patches.Ellipse(gmm.means_[n, :2], v[0], v[1],
                                  180 + angle, color=colors[n])
        ell.set_clip_box(ax.bbox)
        ell.set_alpha(0.5)
        ax.add_artist(ell)


w_step = 0.2
xy_weight = 0.2
while xy_weight <= 1.0:
    z_weight = 0.2
    while z_weight <= 1.0:
        for i in range(3, 7):
            iris_learn = datasets.load_iris()

            csv_f_learn = open('BILO58_5g.xyz_.csv', encoding='utf-8-sig')
            csv_r_learn = csv.reader(csv_f_learn, delimiter=';')
            print('%d clusters' % i)

            step = 1.0 / i

            data_learn = []
            targets_learn = []
            for row in csv_r_learn:
                floats = [float(item) for item in row]
                targets_learn.append(int(floats[2]/step))
                data_learn.append(floats)

            iris_learn.data = np.array(data_learn[:len(data_learn)//100])
            iris_learn.target = np.array(
                targets_learn[:len(targets_learn)//100])

            # Break up the dataset into non-overlapping training (75%) and testing
            # (25%) sets.
            skf = StratifiedKFold(n_splits=4, shuffle=True)

            # Only take the first fold.
            test_index, train_index = next(
                iter(skf.split(iris_learn.data, iris_learn.target)))

            X_train = np.array(data_learn[:len(data_learn)//100])
            y_train = np.array(targets_learn[:len(targets_learn)//100])
            X_test = iris_learn.data[test_index]
            y_test = iris_learn.target[test_index]

            xdata = []
            ydata = []
            zdata = []

            for item in X_test[:len(data_learn)//100]:
                xdata.append(item[0])
                ydata.append(item[1])
                zdata.append(item[2])

            n_classes = len(np.unique(y_train))

            means = [X_train[y_train == j].mean(
                axis=0) for j in range(n_classes)]

            # weights = np.array(make_weights(
            #     points=X_test, means=means, dxy=0.8, dz=0.2))
            # weight_means = np.mean(weights, axis=0)

            estimator = GaussianMixture(
                n_components=n_classes, covariance_type='tied', max_iter=100, random_state=0)

            estimator.means_init = np.array(means)

            g = estimator.fit(X_train)

            y_train_3d_pred = estimator.predict(X_test)
            prob = estimator.predict_proba(X_test)

            probs_w = weighted_probabilities(
                prob, X_test, means, xy_weight, z_weight)
            weighted_pred = map_prob_to_pred(probs_w)
            fig_3d = plt.figure()
            ax = plt.axes(projection='3d')
            ax.scatter3D(xdata, ydata, zdata, c=weighted_pred)

            ax.set_title('heights')
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')

            ax.view_init(30, 120)

            dir = 'c%d' % (i)
            if os.path.exists(dir) == False:
                os.mkdir(dir)

            name = '%s/gmm_xy%.2f_z%.2f.png' % (dir, xy_weight, z_weight)
            fig_3d.savefig(fname=name)
            plt.close()

        z_weight += w_step
    xy_weight += w_step
